package com.hunglp.bmo.dao;

import com.hunglp.bmo.entity.City;

public interface CityRepository extends DefaultRepository<City, Long> {

}
